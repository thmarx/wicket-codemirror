/**
 * Wicket-Codemirror
 * Copyright (C) 2016 Thorsten Marx <kontakt@thorstenmarx.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.thorstenmarx.wicket.codemirror;

import com.thorstenmarx.wicket.codemirror.settings.CodemirrorSettings;
import com.thorstenmarx.wicket.codemirror.settings.Mode;
import java.util.Collection;
import java.util.Collections;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.IAjaxRegionMarkupIdProvider;
import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.http.WebRequest;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.ResourceReference;

/**
 * Renders a component (textarea) as editor, using codemirror.
 * 
 * @author Thorsten Marx (thmarx@gmx.net)
 */
public class CodemirrorBehavior extends Behavior implements IAjaxRegionMarkupIdProvider {

	public static final String VERSION = "5.27.4";
	
	private static final long serialVersionUID = 3L;

	private Component component;
	private CodemirrorSettings settings;
	private boolean rendered = false;

	public CodemirrorBehavior() {
		this(new CodemirrorSettings());
	}

	public CodemirrorBehavior(CodemirrorSettings settings) {
		this.settings = settings;
	}

	@Override
	public void beforeRender(Component component) {
		component.getResponse().write(
				String.format("<div id=\"%s\">", getAjaxRegionMarkupId(component)));
	}

	@Override
	public void afterRender(Component component) {
		component.getResponse().write("</div>");
	}

	@Override
	public void renderHead(Component c, IHeaderResponse response) {
		super.renderHead(c, response);
		if (component == null) {
			throw new IllegalStateException("CodemirrorBehavior is not bound to a component");
		}

		response.render(JavaScriptHeaderItem.forReference(CodemirrorSettings.javaScriptReference()));
		response.render(CssHeaderItem.forReference(CodemirrorSettings.styleSheetReference()));

		for (ResourceReference reference : settings.getMode().getReferences()) {
			if (reference instanceof JavaScriptResourceReference) {
				response.render(JavaScriptHeaderItem.forReference(reference));
			} else if (reference instanceof CssResourceReference) {
				response.render(CssHeaderItem.forReference(reference));
			}
		}
		if (settings.getTheme() != null) {
			for (ResourceReference reference : settings.getTheme().getReferences()) {
				if (reference instanceof JavaScriptResourceReference) {
					response.render(JavaScriptHeaderItem.forReference(reference));
				} else if (reference instanceof CssResourceReference) {
					response.render(CssHeaderItem.forReference(reference));
				}
			}
		}

		String renderOnDomReady = getAddCodemirrorSettingsScript(settings.getMode(),
				Collections.singletonList(component));
		response.render(wrapCodemirrorSettingsScript(renderOnDomReady, component));
	}

	/**
	 * Wrap the initialization script for CodeMirror into a HeaderItem. In this way we can control when and how the script
	 * should be executed.
	 *
	 * @param settingScript the actual initialization script for Codemirror
	 * @param component the target component that must be decorated with Codemirror
	 * @return the HeaderItem containing {
	 * @paramref settingScript}
	 *
	 */
	protected HeaderItem wrapCodemirrorSettingsScript(String settingScript, Component component) {
		return OnDomReadyHeaderItem.forScript(settingScript);
	}

	protected String getAddCodemirrorSettingsScript(Mode mode, Collection<Component> components) {
		StringBuilder script = new StringBuilder();

		for (Component component : components) {
			String id = component.getMarkupId();
			
			String editorName = "cm_" + id;
			script.append("var ").append(editorName).append(" = ").append("  CodeMirror.fromTextArea(document.getElementById(\"").append(id).append("\"), {");
			script.append(settings.toJavaScript(mode, components));
			script.append(" });\r\n");
			script.append(editorName).append(".on('change', function (cme) {");
			script.append("document.getElementById('").append(id).append("').value = cme.getValue();");
			script.append("});\r\n");
		}

		rendered = true;

		return script.toString();
	}

	@Override
	public void bind(Component component) {
		if (this.component != null) {
			throw new IllegalStateException(
					"CodemirrorBehavior can not bind to more than one component");
		}
		super.bind(component);
		if (isMarkupIdRequired()) {
			component.setOutputMarkupId(true);
		}
		this.component = component;
	}

	protected boolean isMarkupIdRequired() {
		return true;
	}

	protected Component getComponent() {
		return component;
	}

	public String getAjaxRegionMarkupId(Component component) {
		return component.getMarkupId() + "_wrapper_component";
	}
}
