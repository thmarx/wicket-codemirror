/**
 * Wicket-Codemirror
 * Copyright (C) 2016 Thorsten Marx <kontakt@thorstenmarx.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.thorstenmarx.wicket.codemirror.settings;

import com.thorstenmarx.wicket.codemirror.CodemirrorBehavior;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.ResourceReference;

/**
 *
 * @author Thorsten Marx (thmarx@gmx.net)
 */
public enum Theme {

	day_3024("3024-day", "codemirror-5.27.4/theme/3024-day.css"),
	night_3024("3024-night", "codemirror-5.27.4/theme/3024-night.css"),
	ambiance("ambiance", "codemirror-5.27.4/theme/ambiance.css"),
	ambiance_mobile("ambiance-mobile", "codemirror-5.27.4/theme/ambiance-mobile.css"),
	base16_dark("base16-dark", "codemirror-5.27.4/theme/base16-dark.css"),
	base16_light("base16-light", "codemirror-5.27.4/theme/base16-light.css"),
	blackboard("blackboard", "codemirror-5.27.4/theme/blackboard.css"),
	cobalt("cobalt", "codemirror-5.27.4/theme/cobalt.css"),
	eclipse("eclipse", "codemirror-5.27.4/theme/eclipse.css"),
	elegant("elegant", "codemirror-5.27.4/theme/elegant.css"),
	erlang_dark("erlang-dark", "codemirror-5.27.4/theme/erlang-dark.css"),
	lesser_dark("lesser-dark", "codemirror-5.27.4/theme/lesser-dark.css"),
	mbo("mbo", "codemirror-5.27.4/theme/mbo.css"),
	midnight("midnight", "codemirror-5.27.4/theme/midnight.css"),
	monokai("monokai", "codemirror-5.27.4/theme/monokai.css"),
	neat("neat", "codemirror-5.27.4/theme/neat.css"),
	paraiso_dark("paraiso-dark", "codemirror-5.27.4/theme/paraiso-dark.css"),
	paraiso_light("paraiso-light", "codemirror-5.27.4/theme/paraiso-light.css"),
	rubyblue("rubyblue", "codemirror-5.27.4/theme/rubyblue.css"),
	solarized("solarized", "codemirror-5.27.4/theme/solarized.css"),
	the_matrix("the-matrix", "codemirror-5.27.4/theme/the-matrix.css"),
	tomorrow_night_eighties("tomorrow-night-eighties", "codemirror-5.27.4/theme/tomorrow-night-eighties.css"),
	twilight("twiligth", "codemirror-5.27.4/theme/twilight.css"),
	vibrant_ink("vibrant-ink", "codemirror-5.27.4/theme/vibrant-ink.css"),
	xq_dark("xq-dark", "codemirror-5.27.4/theme/xq-dark.css"),
	xq_ligh("xq-ligth", "codemirror-5.27.4/theme/xq-light.css");

	private String[] references = null;
	private String name;

	private Theme(String name, String... references) {
		this.name = name;
		this.references = references;
	}

	public Collection<ResourceReference> getReferences() {
		List<ResourceReference> resourceReferences = new ArrayList<ResourceReference>();

		if (references != null) {
			for (String reference : references) {
				if (reference.endsWith(".js")) {
					resourceReferences.add(new JavaScriptResourceReference(CodemirrorBehavior.class, reference));
				} else if (reference.endsWith(".css")) {
					resourceReferences.add(new CssResourceReference(CodemirrorBehavior.class, reference));
				}
			}
		}

		return resourceReferences;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}
}
