/**
 * Wicket-Codemirror
 * Copyright (C) 2016 Thorsten Marx <kontakt@thorstenmarx.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.thorstenmarx.wicket.codemirror.settings;

import com.thorstenmarx.wicket.codemirror.CodemirrorBehavior;
import java.io.Serializable;
import java.util.Collection;
import org.apache.wicket.Component;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.ResourceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Thorsten Marx (thmarx@gmx.net)
 */
public class CodemirrorSettings implements Serializable {

	private static final long serialVersionUID = 3L;
	private static final Logger LOG = LoggerFactory.getLogger(CodemirrorSettings.class);
	public static final ResourceReference JS_REFERENCE = new JavaScriptResourceReference(
			CodemirrorBehavior.class, "codemirror-5.27.4/lib/codemirror.js");
	public static final ResourceReference CSS_REFERENCE = new CssResourceReference(
			CodemirrorBehavior.class, "codemirror-5.27.4/lib/codemirror.css");

	private Mode mode = Mode.HTML_MIXED;

	private Theme theme = null;
	
	private boolean lineNumbers = true;

	public CodemirrorSettings() {
		this(Mode.HTML_MIXED);
	}

	public CodemirrorSettings(Mode mode) {
		this.mode = mode;
	}

	/**
	 * Generates the initialisation script. Internal API, do not call.
	 * @param mode
	 * @param components
	 * @return 
	 */
	public final String toJavaScript(Mode mode, Collection<Component> components) {
		StringBuilder buffer = new StringBuilder();

		// mode
		if (theme != null) {
			buffer.append("\n\t").append("theme : \"").append(theme.getName()).append("\",");
		}
		buffer.append("\n\t").append("mode : \"").append(mode.getName()).append("\",");
		buffer.append("\n\t").append("lineNumbers : ").append(this.lineNumbers);

		return buffer.toString();
	}

	/**
	 * <p>
	 * CodeMirror javascript resource.
	 * </p>
	 *
	 * @return
	 */
	public static ResourceReference javaScriptReference() {
		return JS_REFERENCE;
	}

	public static ResourceReference styleSheetReference() {
		return CSS_REFERENCE;
	}

	/**
	 * @return the mode
	 */
	public Mode getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(Mode mode) {
		this.mode = mode;
	}

	/**
	 * @return the lineNumbers
	 */
	public boolean isLineNumbers() {
		return lineNumbers;
	}

	/**
	 * @param lineNumbers the lineNumbers to set
	 */
	public void setLineNumbers(boolean lineNumbers) {
		this.lineNumbers = lineNumbers;
	}

	/**
	 * @return the theme
	 */
	public Theme getTheme() {
		return theme;
	}

	/**
	 * @param theme the theme to set
	 */
	public void setTheme(Theme theme) {
		this.theme = theme;
	}
}
