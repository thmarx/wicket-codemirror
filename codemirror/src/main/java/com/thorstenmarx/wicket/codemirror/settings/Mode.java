/**
 * Wicket-Codemirror
 * Copyright (C) 2016 Thorsten Marx <kontakt@thorstenmarx.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.thorstenmarx.wicket.codemirror.settings;

import com.thorstenmarx.wicket.codemirror.CodemirrorBehavior;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.ResourceReference;


/**
 * 
 * @author Thorsten Marx (thmarx@gmx.net)
 */
public enum Mode {

	HTML_MIXED("htmlmixed", "codemirror-5.27.4/mode/htmlmixed/htmlmixed.js", "codemirror-5.27.4/mode/xml/xml.js", "codemirror-5.27.4/mode/css/css.js"),
	XML("xml", "codemirror-5.27.4/mode/xml/xml.js", "codemirror-5.27.4/mode/css/css.js"),
	JAVASCRIPT("js", "codemirror-5.27.4/mode/javascript/javascript.js"),
	;

	private String[] references = null;
	private final String name;

	private Mode(String name, String... references) {
		this.references = references;
		this.name = name;
	}

	public Collection<ResourceReference> getReferences() {
		List<ResourceReference> resourceReferences = new ArrayList<>();

		if (references != null) {
			for (String reference : references) {
				if (reference.endsWith(".js")) {
					resourceReferences.add(new JavaScriptResourceReference(CodemirrorBehavior.class, reference));
				} else if (reference.endsWith(".css")) {
					resourceReferences.add(new CssResourceReference(CodemirrorBehavior.class, reference));
				}
			}
		}

		return resourceReferences;
	}
	
	public String getName() {
		return name;
	}


	@Override
	public String toString() {
		return name;
	}
}
