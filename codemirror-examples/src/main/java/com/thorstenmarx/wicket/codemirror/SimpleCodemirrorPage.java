/**
 * Wicket-Codemirror
 * Copyright (C) 2016 Thorsten Marx <kontakt@thorstenmarx.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.thorstenmarx.wicket.codemirror;

import com.thorstenmarx.wicket.codemirror.settings.CodemirrorSettings;
import com.thorstenmarx.wicket.codemirror.settings.Mode;
import com.thorstenmarx.wicket.codemirror.settings.Theme;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.Model;


/**
 * @author Thorsten Marx (thmarx@gmx.net)
 */
public class SimpleCodemirrorPage extends CodemirrorBasePage {

	private static final long serialVersionUID = 1L;

	public SimpleCodemirrorPage() {
		final TextArea textArea = new TextArea("ta", new Model(TEXT));
		
		CodemirrorSettings settings = new CodemirrorSettings();
		settings.setTheme(Theme.base16_dark);
		settings.setMode(Mode.JAVASCRIPT);
		
//		textArea.add(new CodemirrorBehavior(settings));

		Form<?> form = new Form("form") {
			@Override
			protected void onSubmit() {
				System.out.println("input: " + textArea.getModel().getObject());
			}
		};
		form.add(new AjaxSubmitLink("submit") {
			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				System.out.println("input1: " + textArea.getModel().getObject());
//				System.out.println("input2: " + target.textArea.getModel().getObject());
			}
		});
		add(form);

		form.add(textArea);
	}

	private String TEXT = "<!doctype html>\n<html>\n<h2>ueberschrift</h2>\ndas ist einfach\n</html>";
}
