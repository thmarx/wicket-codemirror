/**
 * Wicket-Codemirror
 * Copyright (C) 2016 Thorsten Marx <kontakt@thorstenmarx.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
package com.thorstenmarx.wicket;

import com.thorstenmarx.wicket.codemirror.CodemirrorBasePage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Panel;

/**
 * Navigation panel for the examples project.
 *
 * @author Thorsten Marx (thmarx@gmx.net)
 */
public final class WicketExampleHeader extends Panel {

	private static final long serialVersionUID = 1L;

	/**
	 * Construct.
	 *
	 * @param id id of the component
	 * @param exampleTitle title of the example
	 */
	public WicketExampleHeader(String id, String exampleTitle) {
		super(id);
		add(new Label("exampleTitle", exampleTitle));
		add(new BookmarkablePageLink("homelink", CodemirrorBasePage.class));
	}
}
